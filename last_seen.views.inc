<?php

/**
 * Implements hook_views_data_alter().
 */
function last_seen_views_data_alter(array &$data) {
  /** @var \Drupal\last_seen\LastSeenManager $last_seen_manager */
  $last_seen_manager = \Drupal::service('last_seen.manager');
  $trackers = \Drupal\last_seen\Entity\LastSeen::loadMultiple(NULL);
  foreach ($trackers as $id_tracker => $tracker) {
    /** @var \Drupal\last_seen\Entity\LastSeen $tracker */
    $entity_types_allowed = $tracker->getEntityTypesAllowed();
    foreach ($entity_types_allowed as $entity_type_id => $settings) {
      $data[$entity_type_id]['last_seen_' . $id_tracker]['filter'] = [
        'title' => t('Last seen tracker ' . $tracker->label()),
        'id' => 'last_seen',
        'tracker' => $id_tracker,
        'real field' => $data[$entity_type_id]['table']['join'][$entity_type_id . '_field_data']['field'],
      ];
      $data[$entity_type_id]['last_seen_' . $id_tracker]['sort'] = [
        'title' => t('Last seen tracker ' . $tracker->label()),
        'id' => 'last_seen',
        'tracker' => $id_tracker,
        'real field' => $data[$entity_type_id]['table']['join'][$entity_type_id . '_field_data']['field'],
      ];
    }
  }
}
