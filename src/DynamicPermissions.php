<?php

namespace Drupal\last_seen;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\last_seen\Entity\LastSeen;
use Drupal\last_seen\Entity\LastSeenInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for each media type.
 */
class DynamicPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * MediaPermissions constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * Returns an array of permissions.
   *
   * @return array
   *   The permissions.
   *
   * @see \Drupal\user\PermissionHandlerInterface::getPermissions()
   */
  public function getPermissions() {
    $perms = [];
    // Generate permissions for all last seen types.
    $trackers = LastSeen::loadMultiple(NULL);
    foreach ($trackers as $tracker) {
      /** @var \Drupal\last_seen\Entity\LastSeenInterface $tracker */
      $perms += $this->buildPermissions($tracker);
    }
    return $perms;
  }

  /**
   * Returns a list of media permissions for a given media type.
   *
   * @param \Drupal\last_seen\Entity\LastSeenInterface $tracker
   *   The last seen tracker config entity.
   *
   * @return array
   *   An associative array of permission names and descriptions.
   */
  protected function buildPermissions(LastSeenInterface $tracker) {
    $permissions = [];
    $id = $tracker->id();
    foreach ($tracker->getEntityTypesAllowed() as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        $permissions["tracker $id track $entity_type $bundle"] = [
          'title' => $this->t('Tracker %id: Track entity type %entity_type bundle %bundle', [
            '%id' => $id,
            '%entity_type' => $entity_type,
            '%bundle' => $bundle,
          ]),
        ];
      }
    }
    return $permissions;
  }

}
