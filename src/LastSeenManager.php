<?php

namespace Drupal\last_seen;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\last_seen\Entity\LastSeen;
use Drupal\sessions\CurrentSessionEntity;
use Drupal\sessions\Entity\Sessions;

/**
 * Class LastSeenManager.
 */
class LastSeenManager implements LastSeenManagerInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $currentRouteMatch;

  /**
   * The current session service.
   *
   * @var \Drupal\sessions\CurrentSessionEntity
   */
  protected $currentSessions;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new LastSeenManager object.
   */
  public function __construct(RouteMatchInterface $current_route_match, CurrentSessionEntity $current_session, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentRouteMatch = $current_route_match;
    $this->currentSessions = $current_session;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function trackContent(ContentEntityBase $entity, array &$build) {
    $canonical = $entity->getEntityType()->getLinkTemplate('canonical');
    if (empty($canonical) || $entity->id() === NULL) {
      return;
    }
    $url = $entity->toUrl();
    if ($url->getRouteName() === $this->currentRouteMatch->getRouteName()) {
      foreach ($this->currentRouteMatch->getParameters() as $param) {
        if ($param instanceof ContentEntityBase && $param->getEntityTypeId() === $entity->getEntityTypeId() && $param->bundle() === $entity->bundle() && $param->id() === $entity->id()) {
          $trackers = $this->getTrackersByEntity($entity->getEntityTypeId(), $entity->bundle());
          if (!empty($trackers)) {
            $this->track($entity, $build, $trackers);
            return;
          }
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function track(ContentEntityBase $entity, array &$build, array $trackers) {
    $build['#attached']['library'][] = 'last_seen/mark-as-seen';
    foreach ($trackers as $tracker_id => $tracker) {
      $entity_type = $entity->getEntityTypeId();
      $entity_id = $entity->id();
      /** @var \Drupal\Core\Url $url */
      $url = Url::fromRoute('last_seen.mark_entity', [
        'entity_type' => $entity_type,
        'entity_id' => $entity_id,
        'tracker' => $tracker_id,
      ]);
      if ($url->access()) {
        $build['#attached']['drupalSettings']['last_seen'][$tracker_id] = [
          'entity_type' => $entity_type,
          'entity_id' => $entity_id,
        ];
      }
    }
  }

  public function getTrackersByEntity(string $entity_type, string $bundle = NULL) {
    $trackers = [];
    $items = LastSeen::loadMultiple(NULL);
    /** @var LastSeen $item */
    foreach ($items as $item) {
      $allowed = $item->getEntityTypesAllowed();
      foreach ($allowed as $entity_type_id => $bundles) {
        if ($entity_type === $entity_type_id && (!empty($bundles[$bundle]) || $bundle === NULL)) {
          $trackers[$item->id()] = $item;
        }
      }
    }
    return $trackers;
  }

  /**
   * {@inheritdoc}
   */
  public function markContentAsSeen(ContentEntityBase $entity, LastSeen $tracker) {
    $session_bundle = $tracker->get('sessions');
    $field_name = $tracker->get('field_name');
    $value = [
      'target_type' => $entity->getEntityTypeId(),
      'target_id' => $entity->id(),
    ];
    $session_entity = $tracker->getCurrentUserSessionEntity();
    if ($session_entity === NULL) {
      $session_entity = $this->entityTypeManager->getStorage('sessions')
        ->create([
          'type' => $session_bundle,
          $field_name => [$value],
        ]);
    }
    else {
      $this->mark($session_entity, $field_name, $value);
    }
    $session_entity->save();
  }

  public function mark(Sessions $session_entity, string $field_name, array $value) {
    $values = $session_entity->get($field_name)->getValue();
    array_unshift($values, $value);
    $session_entity->set($field_name, $values);
  }

}
