<?php

namespace Drupal\last_seen\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dynamic_entity_reference\Plugin\DataType\DynamicEntityReference;
use Drupal\field\Entity\FieldConfig;
use Drupal\sessions\Entity\SessionsType;

/**
 * Class LastSeenForm.
 */
class LastSeenForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $last_seen = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $last_seen->label(),
      '#description' => $this->t("Label for the Last seen."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $last_seen->id(),
      '#machine_name' => [
        'exists' => '\Drupal\last_seen\Entity\LastSeen::load',
      ],
      '#disabled' => !$last_seen->isNew(),
    ];

    $form['sessions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sessions'),
      '#description' => $this->t('Sessions bundle machine name'),
      '#default_value' => $last_seen->get('sessions'),
      '#required' => TRUE,
    ];

    $form['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field name'),
      '#description' => $this->t('Sessions dynamic entity reference field name'),
      '#default_value' => $last_seen->get('field_name'),
      '#required' => TRUE,
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $values = $form_state->getValues();
    /** @var SessionsType $sessions */
    $sessions = \Drupal::entityTypeManager()
      ->getStorage('sessions_type')
      ->load($values['sessions']);
    if (!$sessions instanceof SessionsType) {
      $form_state->setErrorByName('sessions', $this->t('The sessions entity bundle does not exists.'));
    }
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('sessions', $values['sessions']);
    if (empty($fields[$values['field_name']]) || !$fields[$values['field_name']] instanceof FieldConfig) {
      $form_state->setErrorByName('field_name', $this->t('The field name is not a valid field.'));
    }
    /** @var FieldConfig $field */
    $field = $fields[$values['field_name']];
    if($field->getType() !== 'dynamic_entity_reference'){
      $form_state->setErrorByName('field_name', $this->t('The field is not a Dynamic entity reference field type.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $last_seen = $this->entity;
    $status = $last_seen->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()
          ->addMessage($this->t('Created the %label Last seen.', [
            '%label' => $last_seen->label(),
          ]));
        break;

      default:
        $this->messenger()
          ->addMessage($this->t('Saved the %label Last seen.', [
            '%label' => $last_seen->label(),
          ]));
    }
    $form_state->setRedirectUrl($last_seen->toUrl('collection'));
  }

}
