<?php

namespace Drupal\last_seen\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\last_seen\Entity\LastSeen;
use Drupal\last_seen\LastSeenManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Mark entities as seen.
 */
class AjaxController extends ControllerBase {

  /**
   * The last seen manager.
   *
   * @var \Drupal\last_seen\LastSeenManagerInterface
   */
  protected $lastSeenManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(LastSeenManagerInterface $last_seen_manager, EntityTypeManagerInterface $entity_type_manager) {
    $this->lastSeenManager = $last_seen_manager;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('last_seen.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Store the id of this entity in session.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @param string $entity_type
   * @param string $entity_id
   * @param string $tracker
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function markEntity(Request $request, string $entity_type, string $entity_id, string $tracker): JsonResponse {
    $entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($entity_id);
    $last_seen = LastSeen::load($tracker);
    if($entity instanceof ContentEntityBase && $last_seen instanceof LastSeen){
      $this->lastSeenManager->markContentAsSeen($entity, $last_seen);
    }

    return new JsonResponse();
  }


  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @param string $entity_type
   *   The entity type.
   *
   * @param string $entity_id
   *   The entity id.
   *
   * @param string $tracker
   *   The last seen tracker id.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(AccountInterface $account, string $entity_type, string $entity_id, string $tracker): \Drupal\Core\Access\AccessResultInterface {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    $bundle = '';
    if ($entity instanceof ContentEntityBase) {
      $bundle = $entity->bundle();
    }
    $permission = "tracker $tracker track $entity_type $bundle";
    return AccessResult::allowedIf($account->hasPermission($permission));
  }

}
