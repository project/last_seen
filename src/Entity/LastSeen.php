<?php

namespace Drupal\last_seen\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Last seen entity.
 *
 * @ConfigEntityType(
 *   id = "last_seen",
 *   label = @Translation("Last seen tracker"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\last_seen\LastSeenListBuilder",
 *     "form" = {
 *       "add" = "Drupal\last_seen\Form\LastSeenForm",
 *       "edit" = "Drupal\last_seen\Form\LastSeenForm",
 *       "delete" = "Drupal\last_seen\Form\LastSeenDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\last_seen\LastSeenHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "last_seen",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/last_seen/{last_seen}",
 *     "add-form" = "/admin/structure/last_seen/add",
 *     "edit-form" = "/admin/structure/last_seen/{last_seen}/edit",
 *     "delete-form" = "/admin/structure/last_seen/{last_seen}/delete",
 *     "collection" = "/admin/structure/last_seen"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "sessions",
 *     "field_name"
 *   }
 * )
 */
class LastSeen extends ConfigEntityBase implements LastSeenInterface {

  /**
   * The Last seen ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Last seen label.
   *
   * @var string
   */
  protected $label;

  /**
   * The Sessions entity bundle.
   *
   * @var string
   */
  protected $sessions;

  /**
   * The field name.
   *
   * @var string
   */
  protected $field_name;

  public function getSessionBundle() {
    return $this->entityTypeManager()
      ->getStorage('sessions_type')
      ->load($this->sessions);
  }

  public function getEntityTypesAllowed() {
    $allowed = [];
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $fields = $entityFieldManager->getFieldDefinitions('sessions', $this->sessions);
    $field = $fields[$this->field_name];
    if (empty($field)) {
      return $allowed;
    }
    $settings = $field->getSettings();
    foreach ($settings['entity_type_ids'] as $entity_type_id) {
      $allowed[$entity_type_id] = $settings[$entity_type_id]['handler_settings']['target_bundles'];
    }
    return $allowed;
  }

  public function getCurrentUserSessionEntity() {
    return \Drupal::service('sessions.current')
      ->getCurrentUserSessionEntity($this->sessions);
  }

}
