<?php

namespace Drupal\last_seen;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\last_seen\Entity\LastSeen;

/**
 * Interface LastSeenManagerInterface.
 */
interface LastSeenManagerInterface {

  public function track(ContentEntityBase $entity, array &$build, array $trackers);

  public function trackContent(ContentEntityBase $entity, array &$build);

  public function markContentAsSeen(ContentEntityBase $entity, LastSeen $tracker);

}
