<?php

namespace Drupal\last_seen\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\last_seen\Entity\LastSeen;
use Drupal\sessions\Entity\Sessions;
use Drupal\views\Plugin\views\filter\InOperator;

/**
 * Simple filter to handle matching of multiple options selectable via
 * checkboxes
 *
 * Definition items:
 * - options callback: The function to call in order to generate the value
 * options. If omitted, the options 'Yes' and 'No' will be used.
 * - options arguments: An array of arguments to pass to the options callback.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("last_seen")
 */
class LastSeenFilter extends InOperator {

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    parent::valueForm($form, $form_state);
    $form['value'] = [
      '#type' => 'hidden',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $this->getValueOptions();
    $errors = parent::validate();

    // If the operator is an operator which doesn't require a value, there is
    // no need for additional validation.
    if (in_array($this->operator, $this->operatorValues(0))) {
      return [];
    }

    if (!in_array($this->operator, $this->operatorValues(1))) {
      $errors[] = $this->t('The operator is invalid on filter: @filter.', ['@filter' => $this->adminLabel(TRUE)]);
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->value = [];
    $tracker = LastSeen::load($this->definition['tracker']);
    if($tracker !== NULL){
      $field_name = $tracker->get('field_name');
      $session_entity = $tracker->getCurrentUserSessionEntity();
      if ($session_entity instanceof Sessions && $session_entity->hasField($field_name) && !$session_entity->get($field_name)
          ->isEmpty()) {
        $values = $session_entity->get($field_name)->getValue();
        foreach ($values as $value) {
          if ($value['target_type'] === $this->table) {
            $this->value[] = $value['target_id'];
          }
        }
      }
    }

    if (empty($this->value)) {
      $this->value = [0];
    }
    $info = $this->operators();
    if (!empty($info[$this->operator]['method'])) {
      $this->{$info[$this->operator]['method']}();
    }
  }

}
