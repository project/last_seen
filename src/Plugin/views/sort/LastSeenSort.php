<?php

namespace Drupal\last_seen\Plugin\views\sort;

use Drupal\last_seen\Entity\LastSeen;
use Drupal\sessions\Entity\Sessions;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Default implementation of the base sort plugin.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("last_seen")
 */
class LastSeenSort extends SortPluginBase {

  /**
   * {@inheritdoc}
   */
  public function usesGroupBy() {
    return FALSE;
  }

  public function query() {
    $this->ensureMyTable();
    $ids = [];
    $tracker = LastSeen::load($this->definition['tracker']);
    if ($tracker !== NULL) {
      $field_name = $tracker->get('field_name');
      $session_entity = $tracker->getCurrentUserSessionEntity();
      if ($session_entity instanceof Sessions && $session_entity->hasField($field_name) && !$session_entity->get($field_name)
          ->isEmpty()) {
        $values = $session_entity->get($field_name)->getValue();
        foreach ($values as $value) {
          if ($value['target_type'] === $this->table) {
            $ids[] = $value['target_id'];
          }
        }
      }
    }
    if (!empty($ids)) {
      $field = $this->tableAlias . '.' . $this->realField;
      $alias = $this->realField . '_' . $this->position;
      $v = implode("','", $ids);
      $expression = "FIELD($field, '$v')";
      $this->query->addOrderBy(NULL, $expression, $this->options['order'], $alias);
    }
  }

}
