(function ($, window, Drupal, drupalSettings) {
  window.addEventListener('load', function () {
    if (drupalSettings.last_seen) {
      Object.keys(drupalSettings.last_seen).forEach(Drupal.last_seen.markAsSeen);
    }
  });

  Drupal.last_seen = {
    markAsSeen: function markAsRead(last_seen_id) {
      var entity_type = drupalSettings.last_seen[last_seen_id]['entity_type'];
      var entity_id = drupalSettings.last_seen[last_seen_id]['entity_id'];
      var n = Math.round(new Date().getTime()/1000);
      $.ajax({
        url: Drupal.url('last_seen/' + entity_type + '/' + entity_id + '/' + last_seen_id + '?n=' + n),
        type: 'POST',
        dataType: 'json',
        success: function success(timestamp) {}
      });
    },
  };

})(jQuery, window, Drupal, drupalSettings);
